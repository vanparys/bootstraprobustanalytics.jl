.. jl:module:: Smoothers

==================
Smoother Functions
==================

.. jl:abstract:: Smoother

The smoother function module provides a variety of common positive smoother functions :math:`S:\Re_+\to\Re_+` commonly employed when using Nadaraya-Watson or generalized nearest neighbors learning. These functions are also commonly referred to as `windowing functions or kernels <https://en.wikipedia.org/wiki/Kernel_%28statistics%29>`_.

Example code::
  
  using Gadfly
  using DataFrames

  using Smoothers

  S_all = [uniform_smoother,
           triangular_smoother,
           epanechnikov_smoother,
           quartic_smoother,
           triweight_smoother,
           tri_cubic_smoother,
           gaussian_smoother,
           cosine_smoother,
           logistic_smoother,
           sigmoid_smoother]
	   
  X = linspace(-3, 3, 1000)
  R = DataFrame(X=[], Y=[], S=[])
  for S in S_all
    for x in X
      push!(R, [x, S(abs(x)), S.name])
    end
  end

  P = plot(R, x=:X, y=:Y, color=:S, Geom.line, Coord.cartesian(ymax=1.1))
  draw(SVG("smoothers.svg", 9.3inch, 7inch), P)

.. image:: smoothers.svg

.. jl:abstract:: Weighter

A weighter function :math:`S_n(x, \bar x) = S(\norm{x-\bar x}/h_n)` consists of a smoother function and a bandwidth parameter :math:`h_n\in\Re_{++}`.

Example code::

  using Smoothers

  S = gaussian_smoother
  d = (x1, x_2) -> norm(x1-x2)
  hn = 0.1
  
  Sn = Weighter(S, d, hn)
  
Uniform Smoother
----------------

.. jl:type:: uniform_smoother <: Smoother

.. math::
   S(\norm{\Delta x}) = \begin{cases} 1 & \mathrm{if} \quad \norm{\Delta x} \leq \frac12 \\ 0 & \mathrm{otherwise} \end{cases}


Triangular Smoother
-------------------

.. jl:type:: triangular_smoother <: Smoother

.. math::
   S(\norm{\Delta x}) = \begin{cases} 1-\norm{\Delta x} & \mathrm{if} \quad \norm{\Delta x} \leq 1 \\ 0 & \mathrm{otherwise} \end{cases}

Epanechnikov Smoother
---------------------

.. jl:type:: epanechnikov_smoother <: Smoother

.. math::
   S(\norm{\Delta x}) = \begin{cases} \frac34 (1-\norm{\Delta x})^2 & \mathrm{if} \quad \norm{\Delta x} \leq 1 \\ 0 & \mathrm{otherwise} \end{cases}
   

Quartic Smoother
----------------

.. jl:type:: quartic_smoother <: Smoother

.. math::
   S(\norm{\Delta x}) = \begin{cases} \frac{15}{16} (1-\norm{\Delta x}^2)^2 & \mathrm{if} \quad \norm{\Delta x} \leq 1 \\ 0 & \mathrm{otherwise} \end{cases}


Triweight Smoother
------------------

.. jl:type:: triweigth_smoother <: Smoother

.. math::
   S(\norm{\Delta x}) = \begin{cases} \frac{35}{32} (1-\norm{\Delta x}^2)^3 & \mathrm{if} \quad \norm{\Delta x} \leq 1 \\ 0 & \mathrm{otherwise} \end{cases}


Tri Cubic Smoother
------------------

.. jl:type:: tricubic_smoother <: Smoother

.. math::
   S(\norm{\Delta x}) = \begin{cases} \frac{70}{81} (1-\norm{\Delta x}^3) & \mathrm{if} \quad \norm{\Delta x} \leq 1 \\ 0 & \mathrm{otherwise} \end{cases}


Gaussian Smoother
-----------------

.. jl:type:: gaussian_smoother <: Smoother

.. math::
   S(\norm{\Delta x}) = {\exp(-\norm{\Delta x}^2)}/{\sqrt(2\pi)}


Cosine Smoother
---------------

.. jl:type:: cosine_smoother <: Smoother

.. math::
   S(\norm{\Delta x}) = \begin{cases} \frac{\pi}{4} \cos(\frac{\pi}{2} \norm{\Delta x}) & \mathrm{if} \quad \norm{\Delta x} \leq 1 \\ 0 & \mathrm{otherwise} \end{cases}


Logistic Smoother
-----------------

.. jl:type:: logistic_smoother <: Smoother

.. math::
   S(\norm{\Delta x}) = \frac{1}{\exp(\norm{\Delta x})+2+\exp(-\norm{\Delta x})}


Sigmoid Smoother
----------------

.. jl:type:: sigmoid_smoother <: Smoother

.. math::
   S(\norm{\Delta x}) = \frac{2}{\pi} \frac{1}{\exp(\norm{\Delta x})+\exp(-\norm{\Delta x})} 
