module BootstrapRobustAnalytics

# General Dependencies
using Distributions
using MLBase
using LinearAlgebra
using Convex
using ECOS
using SCS

# Smoothers
export Weighter
export Smoother
export naive_smoother
export uniform_smoother
export triangular_smoother
export epanechnikov_smoother 
export quartic_smoother
export triweight_smoother
export tri_cubic_smoother
export gaussian_smoother
export cosine_smoother
export logistic_smoother
export sigmoid_smoother

# NNLearning
export nearest_neighbors_learner
export nearest_neighbors_cv
export nearest_neighbors_v
export nearest_neighbors_bs
export nearest_neighbors_bootstrap_dual
export nearest_neighbors_bootstrap_primal
export bootstrap_radius
export rel_entr_j
export J_feasible

# NWLearning
export nadaraya_watson_learner
export nadaraya_watson_cv
export nadaraya_watson_v
export nadaraya_watson_bootstrap_primal
export nadaraya_watson_bootstrap_dual
export nadaraya_watson_bs

include("NNLearning.jl")
include("NWLearning.jl")
include("Smoothers.jl")

end
