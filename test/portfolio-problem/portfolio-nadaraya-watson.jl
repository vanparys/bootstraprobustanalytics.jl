##############
## Packages ##
##############

using CSV
using DataFrames
using BootstrapRobustAnalytics
using NWPortfolio

if length(ARGS)==0
    run = -1
else
    run = parse(ARGS[1])
end

srand(run+100)

###############
## LOAD DATA ##
###############
X_data = CSV.read("X_nt.csv")
Y_data = CSV.read("Y_nt.csv")

###################
## COST FUNCTION ##
###################
LOSS(z, y) = -dot(z, y)

R = DataFrame(run=[], ts=[], n=[], S=[], h=[], j=[], b=[], profit=[], cost=[], cost_p=[], cost_d=[], status=[], status_p=[], status_d=[])

################
## Split data ##
################
n = 200

perm = shuffle(1:size(X_data, 1))
X_data = X_data[perm, :]
Y_data = Y_data[perm, :]

X_tr = Array(X_data)[1:n, :]
Y_tr = Array(Y_data)[1:n, :]
X_val = Array(X_data)[n+1:end, :]
Y_val = Array(Y_data)[n+1:end, :]

#############################
## Hyperparameter training ##
#############################
f = 5
Sn = nadaraya_watson_cv(Y_tr, X_tr, f, verbose=true)

##############################
## Performance on test data ##
##############################
for i in 1:size(X_val, 1)

    println("#######################")
    println("## Validation Sample: ", i, " ##")
    println("#######################")
    
    xbar = X_val[i, :]
    ybar = Y_val[i, :]
    
    ##########################
    # NW nominal formulation #
    ##########################

    println("################")
    println("## Nominal NN ##")
    println("################")

    c_n, z_n, s_n = portfolio_nw_nominal(Y_tr, X_tr, Sn, xbar)
    
    L = [LOSS(z_n[:], Y_tr[i, :]) for i in 1:size(Y_tr, 1)]

    println("######################")
    println("## NW Cost (PRIMAL) ##")
    println("######################")
    
    c_p, P, s, s_p = nadaraya_watson_bootstrap_primal(L, X_tr, Sn, xbar, 0)

    println("####################")
    println("## NW Cost (Dual) ##")
    println("####################")
    
    c_d, s_d = nadaraya_watson_bootstrap_dual(L, X_tr, Sn, xbar, 0)
    
    push!(R, [run, i, n, Sn.smoother.name, Sn.bandwidth, 0, 1, (z_n'*ybar)[1], c_n, c_p, c_d, s_n, s_p, s_d])

    S = [Sn(X_tr[i, :], xbar) for i in 1:n]
    w = S/sum(S)
    n_eff = minimum(find(s->s>1-1e-5, cumsum(sort(w, rev=true))))
    println("n_eff: ", n_eff)
    
    #########################
    # NW robust formulation #
    #########################
    bs = linspace(1, 0.01, 25)
    for j in 1:length(bs)

        println("###############")
        println("## Robust NW ##")
        println("###############")
        
        # Robustness radius Nadaraya Watson
        r = 1/n*log(1/bs[j])
        
        (c_r, z_r, s_r) = portfolio_nw_bootstrap(Y_tr, X_tr, Sn, xbar, r)
        L = [LOSS(z_r[:], Y_tr[i, :]) for i in 1:size(Y_tr, 1)]

        println("######################")
        println("## NW Cost (PRIMAL) ##")
        println("######################")
        
        c_p, P, s, s_p = nadaraya_watson_bootstrap_primal(L, X_tr, Sn, xbar, r)

        println("####################")
        println("## NW Cost (Dual) ##")
        println("####################")
        
        c_d, s_d = nadaraya_watson_bootstrap_dual(L, X_tr, Sn, xbar, r)
        
        push!(R, [run, i, n, Sn.smoother.name, Sn.bandwidth, j, bs[j], (z_r'*ybar)[1], c_r, c_p, c_d, s_r, s_p, s_d])
        
    end

    #############
    ## Results ##
    #############

    if length(ARGS)==0
        CSV.write("Results/R_nw.csv", R)
    else
        CSV.write("Results/R_nw_"*string(ARGS[1])*".csv", R)
    end

end
