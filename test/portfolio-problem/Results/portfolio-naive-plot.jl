using DataFrames
using CSV
using Gadfly

R_na = reduce(vcat, map(CSV.read, filter(x->contains(x, "_na_"), readdir())))

############
## FILTER ##
############

R_na_f = R_na[find(s->s=="Optimal", R_na[:status]), :]
R_na_f = R_na_f[find(s->abs(s)<=1e-4, R_na_f[:cost] -R_na_f[:cost_p]), :]

R_na_f = R_na_f[find(s->s<1, R_na_f[:b]), :]

# R_na_f = R_na_f[find(s->abs(s)<=1e-2, R_na_f[:cost] -R_na_f[:cost_d]), :]

R_na_fa = by(R_na_f, [:b]) do df
    DataFrame(Profit=mean(df[:profit]), profit_s2 = std(df[:profit])/sqrt(size(df[:profit], 1)), m = size(df[:profit], 1))
end

R_na_fa[:Robustness]=1./R_na_fa[:b]

CSV.write("R_na.csv", R_na_fa)

##########
## PLOT ##
##########

plot(R_na_fa, x=:Robustness, y=:Profit, Geom.line, Geom.point, Scale.x_log10) |> PDF("NA.pdf")
