using DataFrames
using CSV
using Gadfly

R_nw = reduce(vcat, map(CSV.read, filter(x->contains(x, "_nw_"), readdir())))

############
## FILTER ##
############

R_nw_f = R_nw[find(s->s=="Optimal", R_nw[:status]), :]

R_nw_f = R_nw_f[find(s->s>=2, R_nw_f[:j]), :]

R_nw_f = R_nw_f[find(s->abs(s)<=1e-4, R_nw_f[:cost] -R_nw_f[:cost_p]), :]
# R_nw_f = R_nw_f[find(s->abs(s)<=1e-3, R_nw_f[:cost] -R_nw_f[:cost_d]), :]

R_nw_val = R_nw_f[find(s->iseven(s), R_nw_f[:ts]), :]
R_nw_tst = R_nw_f[find(s->!iseven(s), R_nw_f[:ts]), :]


R_nw_val_a = by(R_nw_val, [:b]) do df
    DataFrame(Profit=mean(df[:profit]), Profit_min = mean(df[:profit])-std(df[:profit])/sqrt(size(df[:profit], 1)), Profit_max = mean(df[:profit])+std(df[:profit])/sqrt(size(df[:profit], 1)), m = size(df[:profit], 1))
end

R_nw_tst_a = by(R_nw_tst, [:b]) do df
    DataFrame(Profit=mean(df[:profit]), Profit_min = mean(df[:profit])-std(df[:profit])/sqrt(size(df[:profit], 1)), Profit_max = mean(df[:profit])+std(df[:profit])/sqrt(size(df[:profit], 1)), m = size(df[:profit], 1))
end

R_nw_val_a[:Robustness]=1./R_nw_val_a[:b]
R_nw_val_a[:Type] = "Validation"
R_nw_tst_a[:Robustness]=1./R_nw_tst_a[:b]
R_nw_tst_a[:Type] = "Test"

R_nw_val_a = R_nw_val_a[sortperm(R_nw_val_a[:b], rev=true), :]
R_nw_tst_a = R_nw_tst_a[sortperm(R_nw_tst_a[:b], rev=true), :]
CSV.write("R-nw-val.csv", R_nw_val_a)
CSV.write("R-nw-tst.csv", R_nw_tst_a)

##########
## PLOT ##
##########


plot(vcat(R_nw_val_a, R_nw_tst_a), x=:Robustness, y=:Profit, color=:Type, Geom.line, Geom.point, Scale.x_log10) |> PDF("NW.pdf")
