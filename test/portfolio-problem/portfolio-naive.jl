##############
## Packages ##
##############

using CSV
using DataFrames
using BootstrapRobustAnalytics
using NWPortfolio

if length(ARGS)==0
    run = -1
else
    run = parse(ARGS[1])
end

srand(run+100)

###############
## LOAD DATA ##
###############
X_data = CSV.read("X_nt.csv")
Y_data = CSV.read("Y_nt.csv")

###################
## COST FUNCTION ##
###################
LOSS(z, y) = -dot(z, y)

R = DataFrame(run=[], n=[], S=[], h=[], b=[], profit=[], cost=[], cost_p=[], cost_d=[], status=[], status_p=[], status_d=[])

n = 200

for _ in 1:120

    ################
    ## Split data ##
    ################
    perm = shuffle(1:size(X_data, 1))
    X_data = X_data[perm, :]
    Y_data = Y_data[perm, :]

    X_tr = Array(X_data)[1:n, :]
    Y_tr = Array(Y_data)[1:n, :]
    X_val = Array(X_data)[n+1:end, :]
    Y_val = Array(Y_data)[n+1:end, :]
    
    #############################
    ## Hyperparameter training ##
    #############################
    Sn = Weighter(naive_smoother, (x_1, x_2)->norm(x_1-x_2), 1)

    xbar = X_val[1, :]
    ybar = mean(Array(Y_val), 1)
    
    ##########################
    # NW nominal formulation #
    ##########################

    println("################")
    println("## Nominal NA ##")
    println("################")

    c_n, z_n, s_n = portfolio_nw_nominal(Y_tr, X_tr, Sn, xbar)

    L = [LOSS(z_n[:], Y_tr[i, :]) for i in 1:size(Y_tr, 1)]

    println("######################")
    println("## NA Cost (PRIMAL) ##")
    println("######################")
    
    c_p, P, s, s_p = nadaraya_watson_bootstrap_primal(L, X_tr, Sn, xbar, 0)

    println("####################")
    println("## NA Cost (Dual) ##")
    println("####################")
    
    c_d, s_d = nadaraya_watson_bootstrap_dual(L, X_tr, Sn, xbar, 0)
    
    push!(R, [run, n, Sn.smoother.name, Sn.bandwidth, 1, dot(z_n[:], ybar[:]), c_n, c_p, c_d, s_n, s_p, s_d])

    #########################
    # NW robust formulation #
    #########################
    bs = linspace(1, 0.01, 25)
    for j in 1:length(bs)

        println("###############")
        println("## Robust NA ##")
        println("###############")

        # Robustness radius Nadaraya Watson
        r = 1/n*log(1/bs[j])

        c_r, z_r, s_r = portfolio_nw_bootstrap(Y_tr, X_tr, Sn, xbar, r)
        L = [LOSS(z_r[:], Y_tr[i, :]) for i in 1:size(Y_tr, 1)]

        println("######################")
        println("## NA Cost (PRIMAL) ##")
        println("######################")

        c_p, P, s, s_p = nadaraya_watson_bootstrap_primal(L, X_tr, Sn, xbar, r)

        println("####################")
        println("## NA Cost (Dual) ##")
        println("####################")

        c_d, s_d = nadaraya_watson_bootstrap_dual(L, X_tr, Sn, xbar, r)
        
        push!(R, [run, n, Sn.smoother.name, Sn.bandwidth, bs[j], dot(z_r[:], ybar[:]), c_r, c_p, c_d, s_n, s_p, s_d])
    end
    

    #############
    ## Results ##
    #############

    if length(ARGS)==0
        CSV.write("Results/R_na.csv", R)
    else
        CSV.write("Results/R_na_"*string(ARGS[1])*".csv", R)
    end
end
