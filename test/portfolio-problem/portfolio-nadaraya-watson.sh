#!/bin/bash
#SBATCH -a 201-400
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=16000
#SBATCH --time=0-1:00:00
#SBATCH -p sched_mit_sloan_batch

srun julia portfolio-nadaraya-watson.jl $SLURM_ARRAY_TASK_ID
