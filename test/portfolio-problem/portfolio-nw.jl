##############
## Packages ##
##############

using CSV
using DataFrames
using BootstrapRobustAnalytics

using Gadfly

###############
## LOAD DATA ##
###############
X_data = CSV.read("X_nt.csv")
Y_data = CSV.read("Y_nt.csv")
perm = shuffle(1:size(X_data, 1))
X_data = X_data[perm, :]
Y_data = Y_data[perm, :]

################
## Split data ##
################
n = 200

X_tr = Array(X_data)[1:n, :]
Y_tr = Array(Y_data)[1:n, :]
X_val = Array(X_data)[n+1:end, :]
Y_val = Array(Y_data)[n+1:end, :]

#############################
## Hyperparameter training ##
#############################
f = 5
Sn = nadaraya_watson_cv(Y_tr, X_tr, f, verbose=true)

##########
## Plot ##
##########

Xbar = Array(X_data)
perm = sortperm(Xbar[:, 3])
Xbar = Xbar[perm, :]

Y_p = nadaraya_watson_learner(Y_tr, X_tr, Xbar, Sn)

D1 = DataFrame(X=Xbar[:, 3], Y=Y_p[:, 1])
D1[:name]="ibm"
D2 = DataFrame(X=Xbar[:, 3], Y=Y_p[:, 2])
D2[:name]="air"
D3 = DataFrame(X=Xbar[:, 3], Y=Y_p[:, 3])
D3[:name]="ba"
D4 = DataFrame(X=Xbar[:, 3], Y=Y_p[:, 4])
D4[:name]="fb"

D = vcat(D1,D2,D3,D4)

plot(D, x=:X, y=:Y, color=:name, Geom.line) |> PDF("NW.pdf")
