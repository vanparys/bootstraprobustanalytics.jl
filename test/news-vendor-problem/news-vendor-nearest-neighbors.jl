using DataFrames
using CSV
using Distributions
using PDMats

using BootstrapRobustAnalytics
using NNNewsVendor

include("news-vendor-solution.jl")

ds = 0:2:39
runs = 1:10
if length(ARGS)==0
    d=ds[20]
    run=runs[1]
else
    A = zeros(length(ds), length(runs))
    d = ds[ind2sub(A,parse(ARGS[1]))[1]]
    run = runs[ind2sub(A,parse(ARGS[1]))[2]]
end
srand(100+run)

println("dimension: ", d+2)
println("run: ", run)

R = DataFrame(run=[], n=[], d=[], k=[], j=[], b=[], c_t=[], c=[], c_p=[], c_d=[], status=[], status_p=[], status_d=[])

###################
## GENERATE DATA ##
###################

n = 200

for _ in 1:100

    # Covariates
    TEMPERATURE = Uniform(-5, 40)
    DAY = Categorical(ones(7)/7)
    X_nt = DataFrame(TEMPERATURE=rand(TEMPERATURE, n), DAY=rand(DAY, n))
    μ_nt = 100 + (X_nt[:TEMPERATURE]-20)*2
    μ_nt[find(s->s>=6, X_nt[:DAY])] = μ_nt[find(s->s>=6, X_nt[:DAY])] + 20
    Y_nt = μ_nt + 4*randn(n)
    Y_data = DataFrame(DEMAND=Y_nt[:, 1])
    X_data = hcat(X_nt, DataFrame((randn(size(X_nt, 1), d))))
    X_data = Array(X_data)
    X_tr = X_data[1:n, :]
    Y_tr = Array(Y_data)[1:n, :]

    #######################################
    ## COST FUNCTION NEWS VENDOR PROBLEM ##
    #######################################
    b = 10
    h = 1
    LOSS(z, y) = b*max.(y-z, 0) + h*max.(z-y, 0)

    # CONTEXT OF INTEREST
    xbar = [10, 5]
    xbar = vcat(xbar, zeros(d))

    z_t = decision(xbar[1], xbar[2], b, h)
    c_t(z) = cost_model(z, xbar[1], xbar[2], b, h)

    #####################################
    ## Find an appropriate NN smoother ##
    #####################################
    dst, k, Sn, cv = nearest_neighbors_cv(Y_tr, X_tr, 10, verbose=true)

    ############################
    ## NN nominal formulation ##
    ############################
    c_n, z_n, s_n = news_vendor_nn_nominal(Y_tr, X_tr, dst, k, Sn, xbar, b, h)

    L = [LOSS(z_n, Y_tr[i, :])[1] for i in 1:size(Y_tr, 1)]
    c_p, PR, s, jr, s_p = nearest_neighbors_bootstrap_primal(L, X_tr, dst, k, Sn, xbar, 0)
    c_d, s_d = nearest_neighbors_bootstrap_dual(L, X_tr, dst, k, Sn, xbar, 0)

    push!(R, [run, n, d, k, 0, 1, c_t(z_n), c_n, c_p, c_d, s_n, s_p, s_d])

    bs = logspace(0, -6, 26)
    for j in 1:length(bs)

        r = 1/n*log(1/bs[j])

        ###########################
        ## NN robust formulation ##
        ###########################

        c_r, z_r, s_r = news_vendor_nn_bootstrap(Y_tr, X_tr, dst, k, Sn, xbar, b, h, r)

        L = [LOSS(z_r, Y_tr[i, :])[1] for i in 1:size(Y_tr, 1)]
        c_p, PR, s, jr, s_p = nearest_neighbors_bootstrap_primal(L, X_tr, dst, k, Sn, xbar, r)
        c_d, s_d = nearest_neighbors_bootstrap_dual(L, X_tr, dst, k, Sn, xbar, r)

        push!(R, [run, n, d, k, j, bs[j], c_t(z_r), c_r, c_p, c_d, s_r, s_p, s_d])

    end

    CSV.write("Results/R_nn_"*string(d)*"_"*string(run)*".csv", R)    
    println("R:\n", R[:, [:c_t, :c, :c_p, :status]])

end
