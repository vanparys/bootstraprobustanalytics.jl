using DataFrames
using BootstrapRobustAnalytics
using CSV

using NNNewsVendor

# COST FUNCTION NEWS VENDOR PROBLEM
b = 1
h = 1 
LOSS(z, y) = b*max.(y-z, 0) + h*max.(z-y, 0)

function empirical_disappointment(z, c, LOSS, X, Y, xbar, d, k, Sn; n=0, m=5000, verbose=true)
    if n==0
        n = size(Y, 1)
    end

    if verbose
        println("# Start Empirical Disappointment")
        println("********************************")
    end
    
    Z = [LOSS(z, Y[i, :])[1] for i in 1:size(Y, 1)]
    c_bs = nearest_neighbors_bs(Z, X, xbar, d, k, Sn, n=n, m=m, verbose=verbose)

    if verbose
        println("************************************")
        println("*** End Empirical Disappointment ***")
    end
    
    return length(find(s->s > c, c_bs))/length(c_bs)
end

# CONTEXT OF INTEREST
xbar = [15, 5]'

# NUMBER OF DATA POINTS INCLUDED IN TRAINING SET
if length(ARGS)==0
    ns = [100]
    rs = [2e-3]
    mlt = [1]
else
    ns = 30:30:900
    ns = [ns[parse(Int, ARGS[1])]]
    rs = [2e-3,8e-3]
    mlt = 1:20
end

R = DataFrame(n=[], S=[], k=[], r=[], c=[], c_out=[], b_in=[], b_gar=[], status=[])

for n in ns, _ in mlt

    # LOAD DATA
    X_data = Array(CSV.read("X_nt.csv"))
    Y_data = Array(CSV.read("Y_nt.csv"))

    # Training data
    perm = shuffle(1:size(X_data, 1))
    X_data = X_data[perm, :]
    Y_data = Y_data[perm, :]
    X_tr = X_data[1:n, :]
    Y_tr = Y_data[1:n, :]
    X_val = X_data[n+1:end, :]
    Y_val = Y_data[n+1:end, :]

    # Find an appropriate nw smoother
    d, k, Sn = nearest_neighbors_v(Y_tr, X_tr, verbose=true)

    # NW nominal formulation
    (c_n, z_n, s_n) = news_vendor_nn_nominal(Y_tr, X_tr, d, k, Sn, xbar', b, h, verbose=true)

    # IN-SAMPLE
    b_in = empirical_disappointment(z_n, c_n, LOSS, X_tr, Y_tr, xbar, d, k, Sn, m=1000)
    # OUT-SAMPLE
    c_out = nearest_neighbors_learner([LOSS(z_n, Y_val[i, :])[1] for i in 1:size(Y_val, 1)], X_val, xbar, d, k, Sn, verbose=true)[1]
    push!(R, [n, Sn.smoother.name, k, 0, c_n, c_out, b_in, 1, s_n])

    for r in rs

        (c_r, z_r, s_r) = news_vendor_nn_bootstrap( Y_tr, X_tr, d, k, Sn, xbar', b, h, r, verbose=true)

        # IN-SAMPLE
        b_in = empirical_disappointment(z_r, c_r, LOSS, X_tr, Y_tr, xbar, d, k, Sn, m=round.(Int, 200/exp(-r*n)))
        # OUT-SAMPLE
        c_out = nearest_neighbors_learner([LOSS(z_r, Y_val[i, :])[1] for i in 1:size(Y_val, 1)], X_val, xbar, d, k, Sn, verbose=true)[1]
        b_gar = sum(exp.(-n*[max(rel_entr_j(j, k, n)[1], r) for j in 1:n]))
        push!(R, [n, Sn.smoother.name, k, r, c_r, c_out, b_in, b_gar, s_r])

    end

    # SAVE
    if length(ARGS)==0
        CSV.write("Results/NN-NewsVendor.csv", R)
    else
        CSV.write("Results/NN-NewsVendor-"*ARGS[1]*".csv", R)
    end
    
end

