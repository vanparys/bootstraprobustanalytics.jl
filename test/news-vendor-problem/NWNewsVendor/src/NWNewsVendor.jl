module NWNewsVendor

export news_vendor_nw_nominal
export news_vendor_nw_bootstrap

using ECOS
using SCS
using Convex

using BootstrapRobustAnalytics

"""
    news_vendor_nw_nominal(Y, X, Sn, xbar, b, h[, verbose])

# Arguments

    1. 'Y' : Observational data
    2. 'X' : Covariate data
    3. 'Sn' : Weighter function
    4. 'xbar' : Context of interest
    5. '(b, h)' : Marginal backordering and holding costs
    6. 'verbose' : Verbosity

# Returns

    1. 'val' : Optimal Cost
    2. 'z' : Optimal Order
    3. 's' : Solver status
"""
function news_vendor_nw_nominal(Y, X, Sn, xbar, b, h; verbose=false)

    n = size(Y, 1)
    d_y = size(Y, 2)

    if verbose
        println("# Start News Vendor Nominal NW Formulation")
        println("******************************************")
        println("## Problem Parameters")
        println("1. Backorder cost b = ", b)
        println("2. Holding cost h = ", h)
        println("## Problem dimensions ")
        println("1. Number of samples n = ", n)
        println("2. Label dimension : ", d_y)
        println("## Hyper parameters")
        println("1. Weighter function Sn = ", Sn)
    end

    s = sum([Sn(X[i, :], xbar) for i in 1:n])/n

    # Primal Variables
    z = Variable(d_y)
    L = Variable(n)

    obj = sum([L[i]*(Sn(X[i, :], xbar)/s) for i in 1:n])

    constrs = [z>=0]
    for i in 1:n
        constrs = constrs + [L[i] >= b*max(Y[i, :]-z, 0) + h*max(z-Y[i, :], 0)]
    end

    problem = minimize(obj, constrs)
    solve!(problem, ECOSSolver(verbose=verbose))
    
    if verbose
        println("*** End News Vendor Nominal NN Formulation ***")
        println("**********************************************")
    end

    return problem.optval/n, z.value, problem.status
end

"""
    news_vendor_nw_bootstrap(Y, X, Sn, xbar, b, h, r[, verbose])

# Arguments

    1. 'Y' : Observational data
    2. 'X' : Covariate data
    3. 'Sn' : Weighter function
    4. 'xbar' : Context of interest
    5. '(b, h)' : Marginal backordering and holding costs
    6. 'r' : Robustness parameter
    7. 'verbose' : Verbosity

# Returns

    1. 'val' : Optimal cost
    2. 'z' : Optimal order
    3. 's' : Solver status
"""
function news_vendor_nw_bootstrap(Y, X, Sn, xbar, b, h, r; verbose=false)

    n = size(Y, 1)
    d_y = size(Y, 2)

    if verbose
        println("# Start News Vendor Robust NW Formulation")
        println("*****************************************")
        println("## Problem Parameters")
        println("1. Backorder cost b = ", b)
        println("2. Holding cost h = ", h)
        println("## Problem dimensions ")
        println("1. Number of samples n = ", n)
        println("2. Label dimension : ", d_y)
        println("3. Robustness parameter r = ", r)
        println("## Hyper parameters")          
        println("1. Weighter function Sn = ", Sn)
    end

    s = sum([Sn(X[i, :], xbar) for i in 1:n])/n

    # Primal Variables
    z = Variable(d_y)
    # L = Variable(n)

    # Dual Variables
    α = Variable(1)
    ν = Variable(1)
    u = Variable(n)
    
    constrs = [z>=0, ν>=0, u>=0]
    # for i in 1:n
    #     constrs = constrs + [L[i] >= b*max(Y[i, :]-z, 0) + h*max(z-Y[i, :], 0)]
    # end

    constrs = constrs + [sum(u) <= ν*exp(-r)]
    for i in 1:n
        # constrs = constrs + [(L[i]-α)*(Sn(X[i, :], xbar)/s) <= log_perspective(n*u[i], ν)]
        constrs = constrs + [(b*max(Y[i, :]-z, 0) + h*max(z-Y[i, :], 0)-α)*(Sn(X[i, :], xbar)/s) <= log_perspective(n*u[i], ν)]
    end

    problem = minimize(α, constrs)

    solve!(problem, ECOSSolver(maxit=1000, reltol=1e-12, verbose=verbose))
    # if problem.status!=:Optimal
    #     solve!(problem, SCSSolver(max_iters=50000, reltol=1e-6, verbose=verbose), warmstart=true)
    # end

    if verbose
        println("*** End News Vendor Robust NN Formulation ***")
        println("**********************************************")
    end

    return problem.optval, z.value, problem.status
end

#END MODULE
end
