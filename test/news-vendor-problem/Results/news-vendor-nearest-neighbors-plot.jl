using Gadfly
using DataFrames
using CSV

R_nn = reduce(vcat, map(CSV.read, filter(x->contains(x, "_nn_"), readdir())))

############
## FILTER ##
############

R_nn_f = R_nn[find(s->s!=1, R_nn[:j]), :]

R_nn_f = R_nn_f[find(s->abs(s)<=1, R_nn_f[:c] -R_nn_f[:c_p]), :]
#R_nn_f = R_nn_f[find(s->abs(s)<=1, R_nn_f[:c] -R_nn_f[:c_d]), :]

R_nn_fa = by(R_nn_f, [:d, :b]) do df
    DataFrame(c_t=mean(df[:c_t]), c_t_s2 = std(df[:c_t])/sqrt(size(df[:c_t], 1)), n = size(df[:c_t], 1))
end

# R_nn_far = R_nn_fa[find(s->abs(s-1)>=0.1, R_nn_fa[:b]), :]
R_nn_far = R_nn_fa
R_nn_far_a = by(R_nn_far, :d) do df
    index = indmin(df[:c_t])
    DataFrame(c_t=df[:c_t][index], c_t_min=df[:c_t][index]-1.96*df[:c_t_s2][index], c_t_max=df[:c_t][index]+1.96*df[:c_t_s2][index],
c_t_minus=1.96*df[:c_t_s2][index], c_t_plus=1.96*df[:c_t_s2][index],
              T="NN-R")
end
R_nn_fan = R_nn_fa[find(s->abs(s-1)<=0.1, R_nn_fa[:b]), :]
R_nn_fan_a = by(R_nn_fan, :d) do df
    index = indmin(df[:c_t])
    DataFrame(c_t=df[:c_t][index], c_t_min=df[:c_t][index]-1.96*df[:c_t_s2][index], c_t_max=df[:c_t][index]+1.96*df[:c_t_s2][index],
c_t_minus=1.96*df[:c_t_s2][index], c_t_plus=1.96*df[:c_t_s2][index],
              T="NN-N")
end

R_nn_fa_a = vcat(R_nn_far_a, R_nn_fan_a)

R_nn_far_a = R_nn_far_a[sortperm(R_nn_far_a[:d]), :]
R_nn_fan_a = R_nn_fan_a[sortperm(R_nn_fan_a[:d]), :]

CSV.write("R_nnr.csv", R_nn_far_a)
CSV.write("R_nnn.csv", R_nn_fan_a)


##########
## PLOT ##
##########

R_na = CSV.read("R_na.csv")

plot(R_nn_fa_a, x=:d, y=:c_t, y_min=:c_t_min, y_max=:c_t_max, color=:T, Geom.line, Geom.point, Geom.errorbar, Geom.hline(color=["red","black"], size=[1mm,1mm]), yintercept=[R_na[:c_t][1], 10.675]) |> PDF("NN.pdf")

# R_nn_fa = groupby(R_nn_fa)
