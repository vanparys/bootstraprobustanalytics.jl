using FastGaussQuadrature

function cost_model(z, T, D, b, h)

    μ = 100+(T-20)+20*(D==6||D==7)
    σ = 4
    
    nodes, weights = gausshermite(10000)
    LOSS(z, y) = b*max.(y-z, 0) + h*max.(z-y, 0)

    return dot(weights, LOSS(z, sqrt(2)*σ*nodes+μ))/sqrt(π)
end


function decision(T, D, b, h)

    μ = 100+(T-20)+20*(D==6||D==7)
    σ = 4

    p = b/(b+h)

    return μ+σ*sqrt(2)*erfinv(2*p-1)
end

